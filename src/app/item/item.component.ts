import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Items } from './model/items';
import { NgForm } from '@angular/forms';
import { ItemService } from './item.service';

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss'],
    providers: [ItemService]
})


export class ItemComponent implements OnInit {
    loading = false;
    model: Items;
    items: Items[] = [];
    constructor(private itemService: ItemService,
        private router: Router) {
        this.model = new Items();
    }
    ngOnInit(): void {
        this.model.ItemID = 0;
        this.getItems();
    }
    addItem() {
        this.itemService.addItem(this.model)
            .subscribe(
            data => {
                window.location.reload();
            },
            error => {
                this.loading = false;
            });
    }
    getItems(){
        this.itemService.getItems()
      .then(items => {
        this.items = items;
      });
    }
    getItem(itemId){
        this.itemService.getItem(itemId)
      .then(item => {
        this.model = item;
      });
    }
    onEdit(itemId){
        this.getItem(itemId);
    }
}